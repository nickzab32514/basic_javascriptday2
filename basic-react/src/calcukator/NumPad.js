import React from 'react';

const NumPad = (props) => {
    return (
            <table>
            <tr>
                <td><input type="button" value="1" onClick={ ()=> props.onClickNum(1) } className="eval"/></td>
                <td><input type="button" value="2" onClick={ ()=> props.onClickNum(2) }  className="eval"/> </td>
                <td><input type="button" value="3"onClick={ ()=> props.onClickNum(3) }  className="eval"/> </td>
                <td><input type="button" value="/" onClick={ ()=> props.onClickNum("/") }  className="eval"/> </td>
            </tr>
            <tr>
                <td><input type="button" value="4" onClick={ ()=> props.onClickNum(4) } className="eval"/> </td>
                <td><input type="button" value="5" onClick={ ()=> props.onClickNum(5) } className="eval"/> </td>
                <td><input type="button" value="6" onClick={ ()=> props.onClickNum(6) } className="eval"/> </td>
                <td><input type="button" value="-" onClick={ ()=> props.onClickNum("-") }className="eval"/> </td>
            </tr>
            <tr>
                <td><input type="button" value="7" onClick={ ()=> props.onClickNum(7) } className="eval"/> </td>
                <td><input type="button" value="8" onClick={ ()=> props.onClickNum(8) }  className="eval"/> </td>
                <td><input type="button" value="9" onClick={ ()=> props.onClickNum(9) } className="eval"/> </td>
                <td><input type="button" value="+"onClick={ ()=> props.onClickNum("+") }className="eval" /> </td>
            </tr>
            <tr>
                <td ><input type="button" value="0" onClick={ ()=> props.onClickNum("0") } className="eval" /> </td>
                <td colspan="2"><input type="button" value="=" onClick={ ()=> props.onEqual()} className="eval"/> </td>
                <td><input type="button" value="*"onClick={ ()=> props.onClickNaja("*") } className="eval"/> </td>
            </tr>
            <tr>
                <td colspan="4"><input type="button" value="Clear" onClick={ ()=> props.onClear()} className="eval"/> </td>    
            </tr>
            </table>
    );
}

export default NumPad