import React from 'react';
import Display from './Display';
import NumPad from './NumPad';
import "./Calculator.css";

class Calculator extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            equatation: "",
            result: 0
        }
    }

    onEqual = () => {
        this.result = eval(this.state.equatation)
        this.setState({ equatation: this.state.equatation + "=" + this.result });
    }

    onClear = () => {
        this.setState({ equatation: "" }
        );
    }

    onclickButton = (value) => {
        let lastchar = this.state.equatation.substr(-1);
        if (isNaN(value) && (lastchar == "+" || lastchar == "-" || lastchar == "*" || lastchar == "/")) {
            alert("error")
        }
        else {
            this.setState({
                equatation: this.state.equatation + value
            })
        }
    }
    render() {
        return (
            <div className="mid">
                <Display eq={this.state.equatation} />
                <NumPad onClickNum={this.onclickButton} onEqual={this.onEqual} onClear={this.onClear} />
            </div>
        )
    }
}
export default Calculator

